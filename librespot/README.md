# Librespot

Compile the latest version of [librespot](https://github.com/librespot-org/librespot)
as a standalone binary for a scratch container. It prevents most of the
possible backends which requires external libraries. It still support the
`pipe` and `subprocess` backend.

## Description

The container only contains librespot. The binary is defined as the entrypoint.
Any command provided to the container would then be treated as librespot
arguments.

## Docker-compose

Example docker-compose config to run standalone:
```
librespot:
    build:
        context: ./librespot
        args:
            LIBRESPOT_VERSION: v0.4.1
    restart: unless-stopped
    command: ["--name", "librespot", "--backend", "pipe", "--device", "/snapfifo"]
```
