# docker-mopidy-snapcast

Dockerfile and docker-compose to run mopidy and snapcast

# Configuration
* Copy every example files within `config/` directory
* Copy the example `.env` files
* Update them to match your configuration
* Update the `docker-compose.yml` file, specifically the lines with `#changeit`

## Acknowledgment
Thanks to [leakypixel](https://github.com/leakypixel) for its dockerfiles which
were used as a base. See https://github.com/leakypixel/dockerfiles

## License
MIT
