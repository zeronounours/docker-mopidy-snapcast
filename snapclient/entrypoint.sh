#!/bin/bash

set -eo pipefail
shopt -s nullglob

# Case when running like `run snapclient <arguments>`
if [[ "${1:--}" == -* ]]; then
  exec /usr/bin/snapclient "$@"
fi

exec "$@"
