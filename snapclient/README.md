# Snapclient

Runs the latest version of [snapcast](https://github.com/badaix/snapcast) client
on the debian:bullseye-slim base image.

## Description

The client should be able to run audio. This can be done by mounting `/dev/snd`
into the container.

In addition, the client should be run with data required to connect to the
server, like
`docker run --device /dev/snd snapclient --host 127.0.0.1`.


## Docker-compose

Example docker-compose config to run standalone:
```yaml
snapclient:
    build:
        context: ./snapclient
        args:
            SNAPCAST_ARCH: amd64  # either amd64 or armhf
            SNAPCAST_VERSION: 0.26.0
    restart: unless-stopped
    devices:
        - /dev/snd
    command: ["--host", "127.0.0.1"]
```

To be used pulseaudio:
```yaml
snapclient:
    build:
        context: ./snapclient
        args:
            SNAPCAST_ARCH: amd64  # either amd64 or armhf
            SNAPCAST_VERSION: 0.26.0
    restart: unless-stopped
    devices:
        - /dev/snd
    volumes:
        - /run/pulse:/run/pulse
    command: ["--host", "127.0.0.1", "--player", "pulse"]
```
