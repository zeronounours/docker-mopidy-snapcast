FROM debian:bullseye-slim

# Add the apt.mopidy.com repo for pyspotify
RUN apt-get update && apt-get install --yes --no-install-recommends wget ca-certificates \
  && mkdir -p /etc/apt/keyrings \
  && wget -q -O /etc/apt/keyrings/mopidy-archive-keyring.gpg https://apt.mopidy.com/mopidy.gpg \
  && wget -q -O /etc/apt/sources.list.d/mopidy.list https://apt.mopidy.com/bullseye.list \
  && rm -rf /var/lib/apt/lists/*

# Install python, pip
RUN apt-get update && apt-get install --yes --no-install-recommends \
    python3 python3-dev python3-setuptools python3-pip \
    watch \
  && rm -rf /var/lib/apt/lists/*

# Install Plugins
ARG MOPIDY_PLUGINS_APT="mopidy-podcast-itunes mopidy-podcast mopidy-spotify mopidy-local mopidy-mpd"
ARG MOPIDY_PLUGINS_PIP="Mopidy-Mobile Mopidy-Gmusic Mopidy-Moped Mopidy-Iris Mopidy-Party mopidy-musicbox-webclient Mopidy-Youtube"

# Install APT plugins
RUN if [ -n "$MOPIDY_PLUGINS_APT" ]; then \
    apt-get update && apt-get install --yes --no-install-recommends $MOPIDY_PLUGINS_APT \
    && rm -rf /var/lib/apt/lists/*; \
  fi

RUN if [ -n "$MOPIDY_PLUGINS_PIP" ]; then pip install $MOPIDY_PLUGINS_PIP; fi

EXPOSE 6600 6680
ENV LOCAL_SCAN_INTERVAL=600

COPY ./entrypoint.sh /entrypoint.sh
COPY ./config /config

ENTRYPOINT ["/entrypoint.sh"]
CMD ["--config", "/config/core.conf"]
