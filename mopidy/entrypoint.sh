#!/bin/bash

set -eo pipefail
shopt -s nullglob

# Case when running like `run mopidy <arguments>`
if [[ "${1:--}" == -* ]]; then
  # start background scans
  nohup watch -n "$LOCAL_SCAN_INTERVAL" mopidy "$@" local scan &
  exec mopidy "$@"
fi

exec "$@"
