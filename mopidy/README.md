# Mopidy

Runs the latest mopidy with several plugins (controllable) on a debian slim
base image.

## Description

The entrypoint of the container runs `mopidy local scan` upon startup to update
the local DB and do it continuously. This can be overridden by either mounting
your own startup script at `/entrypoint.sh`, or changing the container
entrypoint, just as you would with any other container.

Default included plugins:
```
Mopidy-Mobile
Mopidy-Podcast-iTunes
Mopidy-Podcast
Mopidy-GMusic
Mopidy-Moped
Mopidy-Iris
Mopidy-Spotify
Mopidy-Party
mopidy-musicbox-webclient
Mopidy-Local
Mopidy-MPD
Mopidy-Youtube
```

These plugins my be overridden by changing the build variables:
- `MOPIDY_PLUGINS_APT` for plugins installed from mopidy APT repository
- `MOPIDY_PLUGINS_PIP` for plugins installed directly with pip

## Docker-compose

Use in a docker-compose.yml:
```
mopidy:
    build:
        context: ./mopidy
    ports:
        - "6680:6680"  # Mopidy-HTTP
        - "6600:6600"  # Mopidy-MPD
    command: ["--config", "/config/core.conf:/config/mopidy.conf"]
    volumes:
        - /path/to/you/music:/media/music
        - /path/to/your/local-storage:/root/.local/mopidy
        - /path/to/your/mopidy.conf:/config/mopidy.conf
    restart: unless-stopped
```
