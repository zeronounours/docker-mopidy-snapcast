#!/bin/bash

set -eo pipefail
shopt -s nullglob

# Case when running like `run snapserver <arguments>`
if [[ "${1:--}" == -* ]]; then
  exec /usr/bin/snapserver "$@"
fi

exec "$@"
