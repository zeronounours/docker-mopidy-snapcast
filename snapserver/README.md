# Snapserver

Runs the latest version of [snapcast](https://github.com/badaix/snapcast) server
on the debian:bullseye-slim base image.

## Description

You can override the default config stored in `/config/snapserver.conf` or
the location of the config file by overriding the docker `COMMAND` like so:
`docker run snapserver -c /path/to/config`.

## Docker-compose

Example docker-compose config to run standalone:
```
snapserver:
    build:
        context: ./snapserver
        args:
            SNAPCAST_ARCH: amd64
            SNAPCAST_VERSION: 0.26.0
    restart: unless-stopped
    ports:
        - '1704:1704' # Stream input
        - '1705:1705' # TCP JSON RPC
        - '1780:1780' # HTTP JSON RPC/snapweb
    environment:
      - 'DBUS_SESSION_BUS_ADDRESS=unix:path=/run/dbus/system_bus_socket' # For avahi publishing
    volumes:
        - 'snapfifo:/var/run/snapcast/'
        - '/path/to/your/snapserver.conf:/config/snapserver.conf'
        - '/path/to/your/snapserver/data/:/var/lib/snapserver'
```

Example docker-compose config to run in:
```
mopidy:
    build:
        context: ./mopidy
    ports:
        - "6680:6680"  # Mopidy-HTTP
        - "6600:6600"  # Mopidy-MPD
    command: ["--config", "/config/core.conf:/config/mopidy.conf:/config/snapcast.conf"]
    volumes:
        - snapfifo:/var/run/snapcast/
        - /path/to/you/music:/media/music
        - /path/to/your/local-storage:/root/.local/mopidy
        - /path/to/your/mopidy.conf:/config/mopidy.conf
    restart: unless-stopped
snapserver:
    build:
        context: ./snapserver
        args:
            SNAPCAST_ARCH: amd64
            SNAPCAST_VERSION: 0.26.0
    restart: unless-stopped
    ports:
        - '1704:1704' # Stream input
        - '1705:1705' # TCP JSON RPC
        - '1780:1780' # HTTP JSON RPC/snapweb
    environment:
      - 'DBUS_SESSION_BUS_ADDRESS=unix:path=/run/dbus/system_bus_socket' # For avahi publishing
    volumes:
        - 'snapfifo:/var/run/snapcast/'
        - '/path/to/your/snapserver.conf:/config/snapserver.conf'
        - '/path/to/your/snapserver/data/:/var/lib/snapserver'
```
